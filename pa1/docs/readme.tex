\documentclass{article}

\usepackage{graphicx}
\usepackage{listings}
\usepackage{float}

\lstset{language=bash}

\title{README}
\author{Noah Goldman}
\date{}

\begin{document}

\maketitle

\section{Usage}

The program for this assignment was written in C++.  It can be built using the Makefile included in the main folder of the extracted archive.

\begin{lstlisting}[frame=single]
make
\end{lstlisting}

\subsection{Client}

The client code can be run with the "-c" flag inputted to the main executable.  Along with it, you must specify the IP address of the server (the "-h" flag) and the path to the file to be transmitted ("-f").  Optionally, the N parameter for the Go-Back-N protocol can be set using "-N".  Setting "-N 1" effectively operates the program using a simple selective repeat protocol, and is also the default if the flag is not set.

\begin{lstlisting}[frame=single]
./sftp -c -h 127.0.0.1 -f test_file
./sftp -c -h 127.0.0.1 -f test_file -N 8
\end{lstlisting}

\subsection{Server}

The server can be run with the same executable by passing the "-s" flag.  The drop rate can optionally be set (it defaults to zero) by using the "-L" flag.

\begin{lstlisting}[frame=single]
./sftp -s
./sftp -s -L 0.2
\end{lstlisting}

\section{Known Bugs}

There are no known bugs with the program.

\section{Testing Results}

The code written for the project was tested in multiple scenarios.  All tests were run with the client running on my local machine, and the server running on a VPS that I own, hosted in Chicago.

\begin{table}[H]
  \centering
  \caption{Effect of loss rate on file transmission time (s) for a 400KB file.}
  \label{loss_rate_data}
  \begin{tabular}{|l|l|l|l|l|l|l|}
    \hline
    Loss Rate & Trial \#1 & Trial \#2 & Trial \#3 & Trial \#4 & Trial \#5 & Average \\ \hline
    $0\%$ & $11.2306$ & $12.408$ & $11.3659$ & $11.8644$ & $11.3375$ & $11.6412$ \\
    $10\%$ & $224.126$ & $228.035$ & $264.243$ & $266.219$ & $299.745$ & $242.474$ \\
    $20\%$ & $578.885$ & $578.71$ & $578.863$ & $578.959$ & $587.213$ & $578.72$ \\ \hline
  \end{tabular}
\end{table}

The first test was done was using the stop-and-wait protocol on a 400KB file (setting "-N 1") and varying the loss rate on the server side to the values $0\%$, $10\%$, and $20\%$.  5 trials were conducted for each case, and the raw data is shown in Table \ref{loss_rate_data} while the plot of the averages is shown in Figure \ref{loss_rate_plot}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{docs/loss_rate.png}
  \caption{Transmission time as a function of loss rate for a 400 KB file.}
  \label{loss_rate_plot}
\end{figure}

As shown in both the sources of data, the transfer time clearly increases as the loss rate increases.  The dependency is also fairly linear, which makes sense when considering the timeout behavior of the program.  As the timeout of 2 seconds will dominate the transfer time for any single packet, increasing the loss rate should provide a very proportional increase in the overall transmission time for any values of the loss rate.

\begin{table}[H]
  \centering
  \caption{Effect of N parameter on transmission time(s) for a 400KB file.}
  \label{go_back_n_data}
  \begin{tabular}{|l|l|l|l|l|l|}
    \hline
    Loss Rate & N & Trial \#1 & Trial \#2 & Trial \#3 & Average \\ \hline
    $0\%$ & 1 & $11.0253$ & $11.3799$ & $11.4049$ & $11.2700$ \\
    & 2 & $7.3823$ & $7.5601$ & $7.5465$ & $7.4963$ \\
    & 4 & $4.5212$ & $4.4396$ & $4.4721$ & $4.4776$ \\
    & 8 & $2.5694$ & $2.4926$ & $2.4768$ & $2.5130$ \\
    & 16 & $1.2986$ & $1.3243$ & $1.5258$ & $1.3829$ \\
    & & & & & \\
    $10\%$ & 1 & $238.404$ & $235.219$ & $231.051$ & $234.891$ \\
    & 2 & $226.049$ & $227.408$ & $231.514$ & $228.324$ \\
    & 4 & $234.255$ & $238.570$ & $232.823$ & $235.216$ \\
    & 8 & $225.035$ & $224.835$ & $224.984$ & $224.951$ \\
    & 16 & $231.394$ & $231.141$ & $231.000$ & $231.178$ \\ \hline
  \end{tabular}
\end{table}

The second test conducted used the Go-Back-N version of the program, and the transfer time was observed for the values of $N=1,2,4,8,16$.  The loss rate was also varied to $10\%$ to see the effect of the N parameter on the transfer time in that case as well.  Data was collected by running 3 trials for each case, and it is shown in Table \ref{go_back_n_data} with the corresponding plot of averages for the 0\% loss rate in Figure \ref{go_back_n_plot_0} and the 10\% loss rate in Figure \ref{go_back_n_plot_1}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{docs/go_back_n_0.png}
  \caption{Effect of the N parameter on transmit time for a 0\% loss rate.}
  \label{go_back_n_plot_0}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{docs/go_back_n_1.png}
  \caption{Effect of the N parameter on transmit time for a 10\% loss rate.}
  \label{go_back_n_plot_1}
\end{figure}

The data for the 0\% loss rate shows an exponential decrease in transmit times as the N parameter increases.  The same experiment repeated for a 10\% loss rate shows effectively no correlation between the N parameter and the total transmit times.  The transmit times are also more than an order of magnitude greater than those seen for a 0\% loss rate.  This result makes sense, as the speed gains achieved by increasing the N parameter will effectively be masked by the delays that occur due to packet loss and the 2 second timeout.  The large variation seen between the average transmit times for the different values of N is likely just due to the randomness of the packet loss.

\end{document}
