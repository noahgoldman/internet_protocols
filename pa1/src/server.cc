#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <random>

#include "server.h"
#include "sftp_socket.h"
#include "packet.h"
#include "stopwatch.h"

Server::Server(double L)
    : DROP_RATE(L) {}

void Server::Serve() {
    SFTPSocket sock(SERVER_PORT);

    if (!sock.is_bound()) {
        if (!sock.Init()) {
            printErrorMessage("Failed to initialize socket");
            return;
        }
    }

    while(1) {
        this->handleConnection(sock);
    }
}

void Server::handleConnection(SFTPSocket &sock) {
    std::string clientaddr;
    unsigned short port;
    if (!this->handshake(sock, clientaddr, port)) {
        this->printErrorMessage("Failed to handshake");
        return;
    }

    this->receive(sock, clientaddr, port);
    std::cout << "Finished transferring file, waiting 5 seconds for the connection to fully close." << std::endl;

    if (!this->close(sock, clientaddr, port)) {
        this->printErrorMessage("Failure while closing");
        return;
    }
    std::cout << "Finished transfer with " << clientaddr << std::endl << std::endl;
}

void Server::receive(SFTPSocket &sock, const std::string &clientaddr,
                     const unsigned short port) {
    Packet data_packet(false, false, false, 0);
    std::ofstream outfile;
    unsigned int seq = 1;
    std::uniform_real_distribution<double> dist(0, 1);
    std::default_random_engine re;
    unsigned int received;
    while (!data_packet.isFinSet()) {
        received = false;
        // Receive the data packet
        while (sock.Recv(data_packet, false)) {
            // Simulate dropping a packet using random chance
            if (dist(re) < DROP_RATE) {
                data_packet = Packet(false, false, false, 0);
                continue;
            }
            received = true;

            // Check if the data packet is the next one
            if ((data_packet.getSeq() - data_packet.getData().size()) == seq) {
                std::vector<char> new_data = data_packet.getData();
                std::string data_str;

                // If this is the first packet, extract the filename and open it
                // for writing
                if (seq == 1) {
                    std::string filename;
                    this->extractFile(new_data, filename, data_str);

                    outfile.open(filename);

                    std::cout << "Transferring " << filename << " from "
                        << clientaddr << std::endl;
                } else {
                    data_str = std::string(new_data.begin(), new_data.end());
                }

                outfile << data_str;
                seq = data_packet.getSeq();
            } else {
                data_packet = Packet(false, false, false, 0);
            }
        }

        if (received) {
            Packet ack(false, true, data_packet.isFinSet(), seq);
            if (!sock.Send(clientaddr, port, ack)) {
                printErrorMessage("Failed to send ACK");
                return;
            }
        }
    }
}

bool Server::handshake(SFTPSocket &sock, std::string &clientaddr,
                       unsigned short &port) {
    // Wait for a syn
    Packet syn;
    if (!sock.Recv(syn, clientaddr, &port)) {
        printErrorMessage("Failed to recieve SYN");
        return false;
    }

    if (!syn.isSynSet()) {
        printErrorMessage("SYN packet didnt' have SYN set");
        return false;
    }

    Packet ack = Packet(true, true, false, syn.getSeq() + 1);
    if (!sock.Send(clientaddr, port, ack)) {
        printErrorMessage("Failed to send ACK");
        return false;
    }

    std::cerr << "Successful handshake with " << clientaddr << ":" << port
        << std::endl;
    return true;
}

// Wait 5 seconds to see if a FIN packet is being resent.  If one is, ACK
// it and wait again
bool Server::close(SFTPSocket &sock, const std::string &clientaddr,
                   unsigned short port) {
    Packet fin;
    Stopwatch timer;
    timer.Start();
    unsigned short recv_port;
    std::string outaddr;
    while (timer.Value() < 5) {
        if (sock.Recv(fin, outaddr, &recv_port, false)) {
            if (recv_port == port) {
                if (fin.isFinSet()) {
                    Packet reply(false, true, true, fin.getSeq());
                    if (!sock.Send(clientaddr, port, reply)) {
                        printErrorMessage("Failed to send ACK while closing");
                        return false;
                    }
                    timer.Start();
                } else {
                    printErrorMessage("Got a data packet while trying to close.");
                    return false;
                }
            }
        }
    }

    return true;
}

bool Server::extractFile(const std::vector<char> &data, std::string &filename,
                         std::string &outdata) {
    unsigned int len = data[0];
    filename = std::string(data.begin() + 1, data.begin() + 1 + len);
    outdata = std::string(data.begin() + len + 1, data.end());
    return true;
}

void Server::printErrorMessage(const std::string str) {
    std::cerr << str << std::endl;
}
