#include <chrono>

#include "stopwatch.h"

void Stopwatch::Start() {
    start = std::chrono::steady_clock::now();
    started = true;
}

double Stopwatch::Value() {
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::duration<double>>(now - start).count();
}
