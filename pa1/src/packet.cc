#include <vector>
#include <cassert>
#include <iostream>

#include "packet.h"

Packet::Packet(bool syn, bool ack, bool fin, unsigned int seq)
    : syn_(syn),
      ack_(ack),
      fin_(fin),
      seq_(seq) {}

Packet::Packet(bool syn, bool ack, bool fin, unsigned int seq, std::vector<char> data)
    : syn_(syn),
      ack_(ack),
      fin_(fin),
      seq_(seq),
      data_(data) {}

bool Packet::Serialize(std::vector<char> &out) {
    out.clear();

    if (!this->makeHeader(out)) {
        return false;
    }

    if (this->data_.size() > 0) {
        out.insert(out.end(), this->data_.begin(), this->data_.end());
    }
    return true;
}

bool Packet::makeHeader(std::vector<char> &out) {
    // Init the first 8 bytes
    out.assign(HEADER_LENGTH, 0);

    // Make the first control byte
    char control = 0;
    if (this->syn_) {
        control += 4;
    }
    if (this->ack_) {
        control += 2;
    }
    if (this->fin_) {
        control += 1;
    }

    out[0] = control << 5;
    // out[1] is an unused byte (0)

    // Check that the data can fit in 8 bits
    unsigned int len = this->data_.size();
    if (len > getMaxDataLength()) {
        return false;
    }
    out[2] = len >> 8;
    out[3] = len;

    // Include the sequence number (4 bytes)
    for (char i = 0; i < 4; i++) {
        out[4+i] = this->seq_ >> (8 * (3-i));
    }
    return true;
}

bool Packet::Unserialize(const std::vector<char> &data) {
    // The input data should be at least the length of the header
    if (data.size() < HEADER_LENGTH) return false;

    // Parse the control bits
    this->syn_ = data[0] & 0x80;
    this->ack_ = data[0] & 0x40;
    this->fin_ = data[0] & 0x20;

    unsigned int len = 0;
    len += ((unsigned char)data[2] << 8);
    len += (unsigned char)data[3];

    // Verify that the length field is correct
    assert(data.size() == len + HEADER_LENGTH);

    this->seq_ = 0;
    for (char i = 0; i < 4; ++i) {
        this->seq_ += ((unsigned char)data[4+i]) << (8 * (3-i));
    }

    this->data_ = std::vector<char>(data.begin() + HEADER_LENGTH, data.end());
    return true;
}

std::ostream& operator<<(std::ostream& os, const Packet& p) {
    os << "SYN: " << p.isSynSet() << std::endl
        << "ACK: " << p.isAckSet() << std::endl
        << "FIN: " << p.isFinSet() << std::endl
        << "SEQ#: " << p.getSeq() << std::endl;

    std::vector<char> data = p.getData();
    for (unsigned int i = 0; i < data.size(); i++) {
        os << data[i];
    }
    os << std::endl;
    return os;
}
