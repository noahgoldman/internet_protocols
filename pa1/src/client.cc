#include <algorithm>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <ctime>
#include <chrono>
#include <thread>
#include <deque>
#include <queue>
#include <utility>

#include "client.h"
#include "packet.h"
#include "util.h"
#include "sftp_socket.h"
#include "stopwatch.h"
#include "transfer_log.h"

typedef std::chrono::steady_clock::time_point TimePoint;

Client::Client() {
    this->port_ = 0;
}

Client::Client(unsigned int N)
    : N_(N) {}

Client::Client(unsigned int port, unsigned int N)
    : port_(port),
      N_(N) {}

int Client::sftp(const std::string &ip, const std::string &path) {
    SFTPSocket sock = SFTPSocket();

    // Set up the connection
    if (!this->handshake(sock, ip)) {
        return -1;
    }

    std::cout << "Completed handshake with " << ip << std::endl;

    std::vector<char> data;
    if (!readFile(path, data)) {
        std::cerr << "Failed to read " << path << std::endl;
        return -1;
    }
    unsigned int filesize = data.size();

    padFileData(data, path);

    Stopwatch timer;
    timer.Start();
    this->transfer(sock, ip, data);
    double elapsed = timer.Value();

    TransferLog log;
    log.Init();
    log.Log(path, filesize, elapsed);

    std::cout << "Finished transfer in " << elapsed << " seconds." << std::endl;

    return 0;
}

bool Client::transfer(SFTPSocket &sock, const std::string &ip,
                      const std::vector<char> &data) {
    unsigned int maxlen = Packet::getMaxDataLength();

    std::deque<Packet> packets;
    std::queue<TimePoint> transmitTimes;

    unsigned int win_start = 1;
    while (win_start < data.size()) {
        // Push packets into the window until it hits N packets, or until there
        // is no data left to put in
        while (packets.size() <= N_ &&
               !(packets.size() > 0 && packets.back().getSeq() >= data.size())) {

            // The last sequence number is either the sequence number of the
            // last packet in the queue, or the left side of the window if it
            // is empty
            unsigned int last_seq = win_start;
            if (packets.size() > 0) {
                last_seq = packets.back().getSeq();
            }

            unsigned int seq = std::min(last_seq + maxlen, (unsigned int)data.size());

            packets.push_back(Packet(false, false, seq >= data.size(), seq,
                           sliceVector(data, last_seq - 1, seq - 1)));
        }

        // Send unsent packets
        while (transmitTimes.size() < packets.size()) {
            transmitTimes.push(std::chrono::steady_clock::now());
            Packet toSend = packets[transmitTimes.size() - 1];
            if (!sock.Send(ip, SERVER_PORT, toSend)) {
                std::cerr << "Failed to send data packet:" << std::endl << toSend;
            }
        }

        // Check if the leftmost timer has expired, if it has, resend the whole window
        TimePoint now = std::chrono::steady_clock::now();
        if (std::chrono::duration_cast<std::chrono::duration<double> >(now - transmitTimes.front()).count() > 2) {
            std::cerr << "Failed to receive ACK. Resending packets." << std::endl;
            // Reset the transmit times
            clearQueue(transmitTimes);

            while (transmitTimes.size() < packets.size()) {
                transmitTimes.push(std::chrono::steady_clock::now());
                Packet toSend = packets[transmitTimes.size() - 1];
                if (!sock.Send(ip, SERVER_PORT, toSend)) {
                    std::cerr << "Failed to send data packet:" << std::endl << toSend;
                }
            }
        }

        // Look for ACKs
        Packet ack;
        while (sock.Recv(ack, false)) {
            if (ack.isAckSet() && !ack.isSynSet()) {
                win_start = ack.getSeq();
                while (packets.size() > 0 && packets.front().getSeq() <= win_start) {
                    packets.pop_front();
                    transmitTimes.pop();
                }
            }
        }

    }

    return true;
}

bool Client::handshake(SFTPSocket &sock, const std::string &ip) {
    // Bind to the socket if it wasn't done already
    if (!sock.is_bound()) {
        if (!sock.Init()) {
            std::cerr << "Failed to initialize socket";
            return false;
        }
    }

    // Send the syn packet
    Packet syn(true, false, false, INITIAL_SEQ);
    if (!sock.Send(ip, SERVER_PORT, syn)) {
        std::cerr << "Failed to send SYN packet";
        return false;
    }

    // Wait for the corresponding ACK
    Packet ack = Packet();
    if (!sock.Recv(ack)) {
        std::cerr << "Failed to recieve ACK packet during handshake";
        return false;
    }

    // Check that the packet contains the ACK flag set and that seq is 1
    if (!ack.isAckSet() || !ack.isSynSet() || ack.getSeq() != INITIAL_SEQ + 1) {
        std::cerr << "ACK packet was incorrect";
        return false;
    }

    return true;
}

bool Client::readFile(const std::string &path, std::vector<char> &out) {
    std::ifstream is(path.c_str(), std::ifstream::in);
    if (!is) return -1;

    is.seekg(0, std::ios::end);
    std::streampos len = is.tellg();
    is.seekg(0, std::ios::beg);

    char buf[(int)len];
    is.read(buf, len);

    std::string err;
    if (!is) {
        err = "Failed to read " + path;
    }

    is.close();

    if (!err.empty()) {
        std::cerr << err;
        return false;
    }

    out.assign(buf, buf + len);
    return true;
}

// Pad the actual data by putting the filename size at the beginning (one byte),
// then the filename itself.  Finally add an EOF (0) byte at the end
void Client::padFileData(std::vector<char> &data, const std::string &filename) {
    std::vector<char> tmp = data;
    data.clear();

    // Add the filename length (as a single char)
    data.push_back((char)filename.length());

    // Add the filename
    std::copy(filename.begin(), filename.end(), std::back_inserter(data));
    // Re-add the original data
    data.insert(data.end(), tmp.begin(), tmp.end());
    // Add an EOF
    data.push_back((char)0);
}
