#include <string>
#include <fstream>
#include <chrono>
#include <ctime>

#include "transfer_log.h"

TransferLog::TransferLog()
    : filename_("sftp_status_report.txt") {}

TransferLog::TransferLog(const std::string &logname)
    : filename_(logname) {}

TransferLog::~TransferLog() {
    this->file_.close();
}

void TransferLog::Init() {
    this->file_.open(this->filename_.c_str(), std::ofstream::app);
}

void TransferLog::Log(const std::string &filename, unsigned int size,
                      double time) {

    time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::string timestamp = ctime(&now);
    timestamp.erase(timestamp.length() - 1, 1);
    this->file_ << timestamp << ": Transferred " << filename << " (" << size
        << " bytes) in " << time << " seconds" << std::endl;
}
