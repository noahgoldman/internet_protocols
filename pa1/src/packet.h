#ifndef PACKET_H_
#define PACKET_H_

#include <climits>
#include <vector>
#include <iostream>

#define PACKET_SIZE_EXCEEDED 1

class Packet {
  public:
    Packet() {}
    explicit Packet(bool syn, bool ack, bool fin, unsigned int seq);
    explicit Packet(bool syn, bool ack, bool fin, unsigned int seq, std::vector<char> data);

    bool Serialize(std::vector<char> &out);
    bool Unserialize(const std::vector<char> &data);
    static unsigned int getMaxDataLength() { return MAX_DATA; }
    static unsigned int getMaxSize() { return MAX_DATA + HEADER_LENGTH; }

    // getters
    bool isSynSet() const { return this->syn_; }
    bool isAckSet() const { return this->ack_; }
    bool isFinSet() const { return this->fin_; }
    unsigned int getSeq() const { return this->seq_; }
    std::vector<char> getData() const { return this->data_; }

    friend std::ostream& operator<<(std::ostream& os, const Packet& p);

  private:
    bool makeHeader(std::vector<char>&);

    bool syn_;
    bool ack_;
    bool fin_;
    unsigned int seq_;
    std::vector<char> data_;

    static const char HEADER_LENGTH = 8;
    static const unsigned int MAX_DATA = 400;
};

std::ostream& operator<<(std::ostream& os, const Packet& p);

#endif // PACKET_H_
