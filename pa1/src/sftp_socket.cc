#include <string>
#include <vector>

#include "sftp_socket.h"
#include "packet.h"

SFTPSocket::SFTPSocket()
    : UDPSocket() {}

SFTPSocket::SFTPSocket(unsigned int port)
    : UDPSocket(port) {}

bool SFTPSocket::Send(const std::string &ip, const unsigned short port,
                      Packet &p) {
    std::vector<char> data;
    if (!p.Serialize(data)) {
        return false;
    }
    return UDPSocket::Send(ip, port, data);
}

bool SFTPSocket::Recv(Packet &p, std::string &outaddr, unsigned short *port,
                      bool blocking) {
    std::vector<char> buf;
    if (!UDPSocket::Recv(p.getMaxSize(), buf, outaddr, port, blocking)) {
        return false;
    }
    return p.Unserialize(buf);
}

bool SFTPSocket::Recv(Packet &p, bool blocking) {
    std::vector<char> buf;

    std::string discard_addr;
    unsigned short port;
    if (!UDPSocket::Recv(p.getMaxSize(), buf, discard_addr, &port, blocking)) {
        return false;
    }
    return p.Unserialize(buf);
}
