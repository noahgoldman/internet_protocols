#ifndef UDP_SOCKET_H_
#define UDP_SOCKET_H_

#include <string>
#include <vector>

#include <sys/socket.h>
#include <netinet/in.h>

// A basic UDP socket class that only accepts IP addresses
class UDPSocket {
  public:
    UDPSocket();
    explicit UDPSocket(unsigned int port);

    ~UDPSocket();

    bool Init();

    bool Send(const std::string &ip, const unsigned short port,
              const std::vector<char> &data);
    bool Recv(unsigned int max_len, std::vector<char> &out,
              std::string &outaddr, unsigned short *port,
              bool blocking = true);

    bool is_bound() const { return this->fd_ > 0; };

    void Close();
  private:
    unsigned int port_;
    int fd_;

    static void init_sockaddr(struct sockaddr_in *addr);
};

#endif // UDP_SOCKET_H_
