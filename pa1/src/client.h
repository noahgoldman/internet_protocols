#ifndef CLIENT_H_
#define CLIENT_H_

#include <string>
#include <vector>
#include <chrono>

class SFTPSocket;

class Client {
  public:
    Client();
    Client(unsigned int N);
    Client(unsigned int port, unsigned int N);

    int sftp(const std::string &ip, const std::string &path);

  private:
    unsigned int port_;

    const unsigned int INITIAL_SEQ = 0;
    const unsigned int SERVER_PORT = 9093;

    static bool readFile(const std::string &path, std::vector<char> &out);
    static void padFileData(std::vector<char> &data, const std::string&);

    bool handshake(SFTPSocket &sock, const std::string &ip);

    bool transfer(SFTPSocket &sock, const std::string &ip,
                 const std::vector<char> &data);

    const unsigned int N_ = 1;
};

#endif // CLIENT_H_
