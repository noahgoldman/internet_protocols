#ifndef SERVER_H_
#define SERVER_H_

#include <vector>

class SFTPSocket;

class Server {
  public:
    explicit Server() {}
    Server(double L);

    void Serve();

  private:
    void handleConnection(SFTPSocket&);
    bool handshake(SFTPSocket&, std::string &clientaddr, unsigned short &port);
    bool close(SFTPSocket&, const std::string &clientaddr, unsigned short port);

    void receive(SFTPSocket &sock, const std::string &clientaddr,
                         const unsigned short port);

    static void printErrorMessage(const std::string);

    bool extractFile(const std::vector<char> &data,
                             std::string &filename,
                             std::string &outdata);

    const unsigned int SERVER_PORT = 9093;

    const double DROP_RATE = 0;
};

#endif // SERVER_H_
