#ifndef TRANSFER_LOG_H_
#define TRANSFER_LOG_H_

#include <string>
#include <fstream>

class TransferLog {
  public:
    TransferLog();
    TransferLog(const std::string &logname);

    virtual ~TransferLog();

    void Init();

    void Log(const std::string &filename, unsigned int size, double time);

  private:
    const std::string filename_;
    std::ofstream file_;
};

#endif // TRANSFER_LOG_H_
