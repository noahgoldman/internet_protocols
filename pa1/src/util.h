#ifndef UTIL_H_
#define UTIL_H_

#include <vector>
#include <queue>

template <typename T> std::vector<T> sliceVector(const std::vector<T> &vec,
                                       unsigned int i, unsigned int j) {
    std::vector<T> out;
    out.assign(vec.begin() + i, vec.begin() + j);
    return out;
}

template <typename T> void clearQueue(std::queue<T> &q) {
    while (q.size() > 0) q.pop();
}

#endif // UTIL_H_
