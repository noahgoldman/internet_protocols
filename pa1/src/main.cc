#include <iostream>
#include <string>
#include <unistd.h>
#include <cstring>

#include "client.h"
#include "server.h"

int main(int argc, char *argv[]) {
    bool is_client = false;
    bool is_server = false;
    int N = 1;
    double L = 0;
    std::string host, filename;

    int c;
    while ((c = getopt(argc, argv, "h:f:csN:L:")) != -1) {
        switch (c) {
            case 'h':
                host = std::string(optarg);
                break;
            case 'f':
                filename = std::string(optarg);
                break;
            case 'c':
                is_client = true;
                break;
            case 's':
                is_server = true;
                break;
            case 'N':
                N = atoi(optarg);
                break;
            case 'L':
                L = atof(optarg);
                break;
            case '?':
                return 1;
            default:
                abort();
        }
    }


    if (is_client) {
        std::cout << "Transferring " << filename << " with N=" << N << std::endl;
        Client client(static_cast<unsigned int>(N));
        client.sftp(host, filename);
    } else if (is_server) {
        std::cout << "Running on port 9093 with a loss rate of " << L << std::endl;
        Server s = Server(L);
        s.Serve();
    }
}
