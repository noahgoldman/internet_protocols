#ifndef SFTP_SOCKET_H_
#define SFTP_SOCKET_H_

#include <string>
#include <vector>

#include "udp_socket.h"

class Packet;

class SFTPSocket : public UDPSocket {
  public:
    SFTPSocket();
    explicit SFTPSocket(unsigned int port);

    bool Send(const std::string &ip, const unsigned short port, Packet &p);
    bool Recv(Packet &p, std::string &outaddr, unsigned short *port,
              bool blocking = true);
    bool Recv(Packet &p, bool blocking = true);
};

#endif // SFTP_SOCKET_H_
