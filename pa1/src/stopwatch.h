#ifndef STOPWATCH_H_
#define STOPWATCH_H_

class Stopwatch {
  public:
    void Start();
    double Value();

    bool isStarted() const { return started; }

  private:
    std::chrono::steady_clock::time_point start;
    bool started = false;
};

#endif // STOPWATCH_H_
