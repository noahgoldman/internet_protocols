#include <cstring>
#include <string>
#include <vector>
#include <iostream>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "udp_socket.h"

UDPSocket::UDPSocket()
    : port_(0),
      fd_(-1) {}

UDPSocket::UDPSocket(unsigned int port)
    : port_(port),
      fd_(-1) {}

UDPSocket::~UDPSocket() {
    this->Close();
}

bool UDPSocket::Init() {
    int fd;
    if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
       return false;
    }

    // build a sockaddr_in
    struct sockaddr_in addr;
    init_sockaddr(&addr);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(this->port_);

    if (bind(fd, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
        return false;
    }

    // set the internal socket fd to the correct one
    this->fd_ = fd;
    return true;
}

bool UDPSocket::Send(const std::string &ip, const unsigned short port,
                     const std::vector<char> &data) {
    if (!this->is_bound()) {
        return false;
    }

    // create the destination address
    struct sockaddr_in addr;
    init_sockaddr(&addr);
    inet_pton(AF_INET, ip.c_str(), &(addr.sin_addr)); // set the IP address
    addr.sin_port = htons(port);

    int rc = sendto(this->fd_, data.data(), data.size(), 0, (struct sockaddr*)&addr,
                    sizeof(addr));
    return rc == (int)data.size();
}

bool UDPSocket::Recv(unsigned int max_len, std::vector<char> &out,
                    std::string &outaddr, unsigned short *port,
                    bool blocking) {
    if (!this->is_bound()) {
        return false;
    }

    struct sockaddr_in addr;
    socklen_t addrlen = sizeof(addr);

    char buf[max_len];
    int flags = blocking ? 0 : MSG_DONTWAIT;
    int recvlen = recvfrom(this->fd_, buf, max_len, flags,
                           (struct sockaddr*)&addr, &addrlen);

    if (recvlen < 0) return false;

    char ipbuf[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(addr.sin_addr), ipbuf, INET_ADDRSTRLEN);
    outaddr = std::string(ipbuf);
    *port = ntohs(addr.sin_port);

    out.assign(buf, buf + recvlen);
    return true;
}

void UDPSocket::init_sockaddr(struct sockaddr_in *addr) {
    memset((char*)addr, 0, sizeof(*addr));
    addr->sin_family = AF_INET;
}

void UDPSocket::Close() {
    if (close(this->fd_) > 0) {
        this->fd_ = -1;
    }
}
