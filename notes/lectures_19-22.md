# Lectures 20-22

## Wireless Networks
- _Infrastructure Mode_: A network includes a base station that connects mobiles into a wired network.  As the mobiles move around, they change the base station that connects them to the wired network.
- _Ad-hoc Mode_: No base stations are used, nodes simply transmit to other nodes they can reach and route accordingly to reach others.
- Differences between wireless and wired links:
    + Wireless signal strength attenuates as it propagates through air or other matter.
    + Interference can occur between other sources or objects (such as motors?).
- _CDMA (Code Division Multiple Access)_:
    + Unique code is assigned to each user.  All users share the same frequency but each uses their unique code to encode data.
    + Users can transmit over the same channel with minimal intereference if the codes are orthogonal.
- _IEEE 802.11_:
    + IEEE defines the 802.11 standards (a, b, g, n) that have different transfer rates (19.12).  They all use CSMA/CA for multiple access.
    + Hosts communicate with a base station (or access point).
    + AP's operate at one of the 11 channels in the 2.4Ghz-2.485Ghz range.
    + Hosts associate with an AP by scanning channels, listening for beacon frames containing SSID's and MAC addresses, then select one.
    + Hosts can also send a probe request frame on a channel asking for APs to identify themselves.
    + 802.11 uses CSMA for access control (19.17)
- IEEE 802.11 can use the following medium access control mechanisms:
    + *CSMA:* Blindly transmit an entire frame if the channel is sensed as idle.  If the channel is busy, set a timer that is started when the channel is sensed as idle again.  When this timer elapses, re-send the data.
        - There is no collision detection, as it is difficult to check for transmissions while also sending, and the hidden terminal problem could manifest.
    + *CSMA/CA:* Explicit channel reservation is used with RTS (request-to-send) and CTS (clear-to-send) messages. (20.8)
        - Senders send short RTSs.
        - Receivers reply with a CTS to all parties, telling all other hosts to refrain from sending.
        - Since RTS and CTS are so short, collisions are less likely and the penalty of them is smaller.
        - An example is on 20.10
    + *Polling*
- Exposed terminal problem: See 20.11.  This is a problem that causes less spatial reuse.  Since 802.11 is bidirectional, this is not a problem in that procol.
- See 20.12 for 802.11 frame information.
- When a host moves between two APs in a certain subnet, the underlying network switch will see packets from a host on a new port and re-learn where to forward packets.
- The base station dynamically changes transmission rate to match the signal-to-noise ratio. (20.16)
- *802.16 WiMAX:* Much larger range than 802.11 (~6 miles)

## Cellular Networks
- *Cell:* A tower or base station that covers a geographical region.
- *MSC (Mobile Switching Center):* Connects cells to a wide area network (WAN), and handles mobility.
- Cellular network multiplexing (20.23):
    - FDMA/TDMA: Divide spectrum into frequency channels with sub time slots.
    - CDMA: Code division multiple access.  CDMA's performance is most affected by the discrepancy in transmission power among users.
- Mobility in cell networks:
    + *Home Network:* The network of the cellular provider, contains the home location register (HLR) that contains information like permanent cell phone number, and information about a users current location.
    + *Visited Network:* The network in which the mobile currently resides.  Each contains a visitor location register (VLR) that has an entry for each user currently in the network.
        - When a user moves from one MSC to another, the entire VLR doesn't move.
- GSM can handle mobiles moving from one base station to another via a handoff procedure (20.33).  However the first MSC visited is the *anchor MSC* which is always in the signal chain.
- The properties of cellular networks can influence the performance of higher-level protocols.
    + Increased packet loss and delay will occur due to bit errors and the handoff procedure.
    + TCP will interpret loss as congestion, which will cause avoidance code to kick in when not really necessary.

## Mobility
- *Home Network:* Permanent "home" of the mobile.  This is a subnet, for example (128.119.40/24).
- *Home Agent:* An entity that will perform actions on behalf of the mobile, when it is remote.
- *Permanent Address:* The fixed address of a mobile in the home network.  This is an IP address.
- Approaches to mobility:
    + Routers advertise routes to each mobile as they move.
    + Indirect Routing:  Communication to any mobile host is sent through a home agent, then to a foreign agent, then to the final destination (or reverse).  This is the more transparent of the two approaches.
    + Direct Routing: Correspondents simply get the foreign address of a mobile, and communicate directly.
- *Mobile IP (21.15):*
    + Supports indirect routing of datagrams.
    + Agent discovery so a mobile can find a foreign agent or a foreign agent can find a home agent.
    + Includes support for registration so foreign agent can register with a home agent.
    + Uses port 434.

## SNMP
- Simple Network Management Protocol (22.1)
- SNMP agents run on port 161, while clients respond to Traip messages on 162.
- Runs on UDP.
- SMI defines the rules for naming objects and their types, as well as how to encode objects.
- MIB is a collection of named objects in a tree-like structure.
- In an SNMP GetRequest, the VarBind list first specifies the address of the variable, then the type of the value with a length of zero.  See 22.43.
