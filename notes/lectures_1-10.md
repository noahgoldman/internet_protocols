# Lectures 1-10

## IP Addressing
- IPv4 is 32 bits, IPv6 is 128 bits
- IP addresses are associated with an interface, not a device
- Five address classes (1.12):
    - Class A (0.0.0.0 - 127.255.255.255) have 24 bits that can change
    - Class B (128.0.0.0 - 191.255.255.255) can change 16 bits
    - Class C (192.0.0.0 - 223.255.255.255) can change 8 bits
    - Class D (224.0.0.0 - 239.255.255.255) is reserved for multicast addresses
    - Class E (240.0.0.0 - 255.255.255.255) reserved for future use
    - The number of addresses per class is on 1.14
- Classless addressing (1.15) introduces less waste, slows down address space exhaustion
- CIDR (Classless InterDomain Routing): allows for the network portion of an address to have an arbitrary length
    - Takes the form x.x.x.x/y where y is the number of bits in the network portion of the address.
- Network mask (subnet mask) (1.21): Put all 1's in the network part of the address, and 0s in the host part.
    - The 1's must be contiguous
    - Routers use these with the statement `if (mask[i] & destination addr == destination[i]) forwardTo(host[i])`
- To route IP addresses, routers use Longest Prefix Matching (works as it sounds) with a forwarding table.
- All IP address spaces reserve the address with the host portion as 1's for multicast.  This eliminates a potential IP address that can be assigned to an interface from the pool.

## Address Resolution and Host Configuration

### Address Resolution Protocol (ARP)
- ARP (2.5) converts an IP address to a MAC address.
- ARP requests are always broadcast (multicast).
- ARP replies are generally unicast to the sender.
- The packet format is on 2.6
- Messages are sent on the network layer (next to IP) over the link layer.
- The size of an ARP message can vary based on the link and network layers, but for Ethernet and IP it is 28 bytes.
- Hosts receiving an ARP requests cache the IP/MAC address mapping in their ARP cache.
- Proxy ARP introduces an ARP router that responds to ARP requests for certain mosts.
    - This is typically used by a home agent to proxy for a mobile host.
    - Also useful when two physical network segments are connected with a router, but part of the same IP subnet (2.10).
- Gratuious ARP: Host sends an ARP request for its own address.
    - Hosts can use this to determine if another host as the same IP
    - Can help a host tell if its hardware address has changed.
    - Forces other hosts to update their hardware caches.

### Reverse Address Resolution Protocol (RARP)
- Reverse ARP (2.14)
- Used by ROM-only hosts to ask the network for an IP address.
- Requests are broadcast, replies are unicast.
- RARP is more complex than ARP
- RARP requests are not forwarded by routers (they are link-layer only).
- Packet format is on 2.16.
- RARP requires one RARP server in each IP network, and it returns only an IP address.

### BOOTP
- BOOTP packets are over UDP, and can be forwarded by routers.
- Packet format is on 2.23.
- BOOTP requests are broadcast, replies are typically unicast but can be broadcast.
    - This unicast response is done by BOOTP updating the ARP cache manually.
- The client IP address field doesn't have to always be zero.
- BOOTP implementation is system-independent.

### DHCP
- The successor to BOOTP, uses effectively the same packet format (2.27).
    - DHCP adds an F bit in the header that controls weather a reply should be unicast or multicast.
    - The options field also includes the various DHCP options, DHCPDISCOVER, DHCPOFFER, DHCPREQUEST, DHCPDECLINE, DHCPACK, DHCPNACK and DHCPRELEASE
- DHCP clients can renew information periodically by time, rather than simply every time the machine reboots.
- DHCP uses a state machine to decide which type of message is needed.
- DHCP options (2.28), state machine (2.29), and example (2.30) are in the slides.

## Domain Name System
- DNS maps domain names to IP addresses.
- x.x.x.x.in-addr.arpa. corresponds to a reverse DNS lookup.
- Any label in a domain name www.x.com (where x is the label) can be up to 63 chars.
- Depth of the DNS tree can be up to 128 including the root ".".
- Types of domain names:
    - **FQDN**: Ends with a ".".
    - **PQDN**: Initial part of a FQDN, doesn't end with a ".".
- In DNS, a **Zone** is responsible for a certain part of a subtree.
    - Each zone has a primary name server, and multiple secondary name servers are backups that perform routing zone transfers from the primary
- Can do iterative or recursive queries.
- Packet format is on 3.14.
- DNS has many query types.
- DNS uses offset pointers to avoide repeated data.
    - See slide 2.22 for the exact format.
    - Offset pointers are 16 bits, with the first two bits being 1's and the rest being the address.
- Both queries and responses can include 1 or more Resource Records.
- UDP is normally used, TCP is used for messages over 512 bytes.
    - For messages over 512 bytes, UDP is used for the first 512 bytes with the TC flag set, then TCP is used for the rest.
- A DNS query can never be more than 512 bytes (thus the TC flag is alwasys 0).
- Uses port 53.

## ICMP
- Utility protocol that runs on top of IP (4.3).
- Two types of messages: Error-reporting and Query
- Error-reporting Messages:
    - No error message will be generated in certain cases, see 4.8.
    - Include the IP header of the failed datagram, along with the first 8 bytes of data.
    - **Destination Unreachable**: Sent when a router cannot deliver a datagram.
    - **Source Quench**: Potentially sent when a datagram is dropped at a router due to congestion.  This indicates to the sender to slow down.
    - **Time Exceeded**: Sent when the IP TTL field expires, or when the fragmentation reassembly time is exceeded.
- Query/Response Messages: (see 4.15)
    - **Echo Request/Reply**: Used by ping to determine weather hosts can communicate.
    - **Timestamp Request/Reply**: Get a timestamp showing the RTT to a host as well as one-way delay. (4.17)
    - **Mask Request/Reply**: Ask the router with unicast (if known) or through broadcast for the subnet mask of the hosts.
    - **Router Solicitation/Advert Messages**: Used to identify routers on the LAN, through broadcasts.  Routers can also send adverts that are unsolicited.
    - **Redirect Messages**: Sent by a router to a host saying that there is a more optimal route to a certain destination by going through a different gateway.  This is a method of routing on a LAN with multiple routers but no routing protocols in use (BGP/OSPF/RIP).
- The checksum is the IP checksum, add all 16-bit words and take the 1's complement of the sum.

## IPv4
- Main network-layer protocol (5.0)
- Header format is on 5.5.
    - **VER**: 4 for IPv4, 6 for IPv6
    - **HLEN**: Length of the header divided by 4. (length in 4 byte words).
    - Total Length: Data length plus header length.
    - Fragmentation Offset: The offset of the data in the packet from the original data.  Given in units of bytes * 8.
    - Max header length is 60
    - Header length with no options is 20 bytes.
    - The value of the TTL field is 1+ the number of more routers the datagram can traverse.
    - If the TOS field is zero, it is a normal service.
- IP datagrams are fragmented, with the offset being multiplied by 8 to get the real value.
- Uses the IP checksum previously described.
- Strict vs. Loose Source Route Options:
    - Strict Source Route forces the datagram to travel in the specific route.
    - Loose Source Route just means the datagram must visit all IPs in the route.
    - Both change the destination IP address field as the datagram travels.
- Limitations:
    - Shortage of address space
    - Needs better support for real-time audio/video
    - No encryption

## IPv6
- Advantages over IPv4:
    - Smaller fixed 40-byte header
    - New fragmentation procedures
    - Improved options
    - Allows easier extensions
- IPv6 Addresses (6.6):
    - 128 bits
    - Abbreviation rules:
        - Leading 0's of a section can be omitted.
        - Any one run of 0 sections can be abbreviated.
    - Can use CIDR notation (ex. ::/60)
    - Special addresses:
        - Unspecified address: ::/128
        - Loopback address: ::1/128
- IPv6 format is given on 6.12
- Header:
    - Payload length replace total length
    - TTL is called hop limit
    - Checksum is eliminiated
    - Uses the concept of "Next Header" to indicate where either the next header option is, or where the data starts.
- Fragmentation is done using the fragmentation extension header.

## ICMPv6
- Adds the "Packet too big" error message, removes the "Source quench" mesage.
- Miscellaneous other changes in Lecture 6.

## UDP
- UDP header size is fixed at 8 bytes
- Total length field = Length of IP - length of IP header.
- The checksum is computed over the data and the header as well as a pseudo-IP header.
    - To compute the checksum, a fake IP header is created and prepended to the UDP segment.
    - The checksum is then calculated over the entire thing.
    - This protects against errors in the network layer, such as accidental delivery of a mesasge to the wrong destination, wrong protocol, wrong length.

## TCP
- See format on 7.2.
- Header specifics:
    - SEQ: The number of the first byte in the segments data.
    - ACK: The sequence number of the next byte expected from the other side.
- Three way handshake:
    1. Client sends SYN with initial sequence number
    2. Server sends SYN+ACK with the servers initial sequence number and ACKs the clients SEQ
    3. Client ACKs the servers SYN+ACK
- Connection Closing:
    - Three way handshaking (7.10):
        1. One side sends a segment with FIN set.
        2. The other side sends FIN+ACK
        3. The other side responds with ACK.
    - Half closing (7.11):
        1. One side sends a segment with FIN.
        2. The other side ACKs it.
        3. Repeat for the other side.
    - For both methods, there is a TIMED_WAIT stage at the end of the closing process.  The connection is kept open so that if an ACK for a FIN is not received, the FIN can be retransmitted (or reverse for the other side).
- Simultaneous close and open can happen (7.16)
- Packets are retransmitted on:
    - Retransmission timers
    - Three dupACKs
- No retransmission timer is set for ACKs.
- **Silly Window Syndrome**:
    - When the receiver's window becomes so small that extremelly small TCP segments begin being sent, which is very inefficient.
    - This can be solved with Nagle's Algorithm (7.2)
    - Also solved with Clark's Solution.
- RTT Estimation:
    - AvgRTT = (1 - alpha) * AvgRTT + alpha * SampleRTT
    - DevRTT = (1 - beta) * DevRTT + beta * abs(SampleRTT - AvgRTT(most recent))
    - RTO = AvgRTT + k * DevRTT
    - Typical values alpha=0.125, beta=0.25, and k=4.
- TCP Congestion Control (see 7.17):
    - Uses Additive Increase, Multiplicative Decrease
- TCP Options:
    - **EOP**: End-of-options list
    - **NOP**: No operation, used for padding to align fields on 32-bit boundaries.
    - **Maximum Segment Size**: Provides the MSS of the sender.
    - **Window Scale Factor**: Provides a scaling factor for the MSS field.
    - **Timestamp**: Provides the timestamp value, and an echo of the previous timestamp.  This can be used for RTT calculation.
    - **SACK Permitted**: Enables selective acknowledgements, only used during setup.
    - **SACK**: Allows the host to specify blocks of discontinuous data that can be selectively ACKed.

## TFTP
- Simplified version of FTP, uses port 69 (9.3).
- Uses UDP with its own flow control.
- Four messages:
    - **RRQ**: Read request to a file.
    - **WRQ**: Write request to a file.
    - **DATA**: Includes data blocks.
    - **ACK**: Ack.
- No authentication at all.
- Basic stop and wait protocol.
- Sorcerer's Appretice Bug:  When an ACK for a data packet isn't received in time, the server can duplicate the previous packet and possibly cause doubling of all data sent.  This phenomenon can propagate.
    - This problem is fixed by ensuring that retransmissions of apacket only occur on timeouts.
- BOOTP works together with TFTP.
- Advantages over FTP:
    + Runs on UDP.
    + TFTP is used for BOOTP.

## FTP
- Runs on top of TCP.
- Uses a data and control connection, on 20 and 21 respectively.
    - This increases the simplicity of the protocol.
    - It also allows multiple threads to easily use both ports, decreasing latency to control requests.
- FTP uses the PASV command to start the data connection with a client.

## Telnet
- Telnet forks when receiving a new connection request.
- Characters are sent over the network in the Network Virtual Terminal (NVT) format).
    - Data characters begin with a 0 bit.
    - Control characters begin with a 1 bit.
    - The IAC (Interpret as Control) characters 11111111 precedes each control character.
- Telnet can send an out-of-band message with the DM command, while also setting the TCP flag URG.  This causes the segment to be immediately pushed into the application layer, and telnet can then process it first.  This could be set when sending a "^C" character over telnet.
