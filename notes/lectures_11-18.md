# Lectures 11-18

## IGMP
- IGMP allows routers to keep track of hosts that are members of multicast groups on a network.
- It operates at the same network level as IP, ARP, ICMP...
- Multiple routers in the same network must not have the same hosts registered for a multicast group, otherwise that host could receive a multicasted packet many times.
- *Packet Types*:
    + Membership Report (11.6): Sent by a host or router when it going a group from a different router.  This is sent twice to avoid errors with datagrams being dropped.
    + Leave Report: When a member leaves a group.
    + Special Query: Sent by a router to check if any hosts are still interested in a group with no members in it.  If it receives no membership report saying that a host is in the group, it is deleted.
    + General Query: Sent by a router to another host or router, asking for a membership report.
- IGMP messages and corresponding IP Addresses:
    + Query (224.0.0.1): All systems on the subnet
    + Leave Report (224.0.0.2): All routers on the subnet
    + Membership Report (the multicast address of the group)
- Misc:
    + IGMP datagrams sent over IP require that the TTL field is exactly 1
    + The _Maximum Response Time Field_ is in tenths of a second (MRT=200 really means 20 seconds)

## RTSP
- An application layer protocol designed for multimedia A/V transmission
- It includes controls such as PLAY, PAUSE, RESUME...
- Doesn't specify a media format or a transport layer procotol.
- Uses a control channel on a different port from the data channel (port 554) (11.20)
- Uses *Forward Error Correction* (11.23):
    - For every group of n chunk create a redundant chunk by XOR'ing all of the originals.
    - Send this n+1'th chunk, which can be used to recreate a single lost chunk.
- Can also send a lower quality stream alongside the normal one, which can be used as a fallback in the case of packet loss.
- RTSP can also interleave chunks into the same packet, so if a single packet is lost it is almost unnoticable.  This has the downside of introducting more playout delay.

## RTP
- A protocol for carrying AV data.
- RTP runs on top of UDP
- Certain encodings are specified in the RTP header, along with a sequence number (11.27)
- Also uses an out-of-band control connection, in a protocol called RTCP (RTP Control Protocol).
- RTCP provides timestamps for each packet in the RTP stream, allowing audio and video to be easily synced together.
- RTCP tried to limit itself to 5% of the data connection bandwith.
    + 75% of this rate goes to receivers, 25% to sender
- RTCP uses the port number of the RTP session, plus 1
- The maximum number of contributors to an RTP session is 15.
- The maximum size of the RTP header is 12.


## SIP
- Session Initiation Protocol (11.33) is used for signaling and controlling multimedia communication sessions.
- Commonly used for voice and audio calls over IP, as well as instant messaing.
- SIP should be able to deliver to an IP, email, or phone number no matter where in the world or what network the host is in.
- Common functionality:
    + Allows users to set up phone calls and end them
    + Maps another identifier (email, phone number) to an IP address
    - Other call management functions (11.35)
- The ability to find an IP address based on another identifier is provided through SIP registrars and SIP proxy servers.
- When setting up a call, SIP helps the users agree on the encoding type or protocol used for the transmission.

## Cryptography
- _Symmetric Key_: The sender and receiver keys are identical and both secret.
- _Public Key_: The encryption key is public, the decryption key is private.
- Public vs. Symmetric key encryption:
    + Symmetric key encryption is much simpler.  Public key encryption requires a key distribution infrastructure, as potentially an authority to verify that a key belongs to a specific organization or entity.  The actual algorithm is also typically much more complex for public key.
    + In order to use symmetric key encryption the key must be exchanged.  This can be a big security hole if an outside party obtains the key.
- There are multiple potential types of attacks (12.10).
    + Ciphertext-only attack: The intruder can only see the ciphertext
    + Known-plaintext attack: The intruder knows some plaintext,ciphertext pairings, but can't decrypt all
    + Chosen-plaintext attack: The intruder is able to generate ciphertext from arbitrary plaintext.
- _Block Ciphers_: Create a table mapping blocks of k bits to other bits, use that to encrypt data.  More blocks becomes more secure.
- Cipher blocks can be chained to produce a different output, see slide 12.13.
- _PGP (Pretty Good Privacy)_: Is a program that uses uses GPG to provide privacy and authentication for data.
    + It uses symmetric key encryption, and well as asymmetric encryption.
- _DES (Data Encryption Standard) (12.14)_:
    + Uses a 56-bit symmetric key with 64 bit plaintext blocks.
    + Can be brute-forced in a period of time.
    + Iteratively applies the blocks 16 times using different 48 bits of the key
    + _Triple DES (12.16)_: Using 2 or 3 keys, encrypt using K1, decrypt using K2, then encrypt again using K2/K3.
- _AES (Advanced Encryption Standard)_: Uses 128 bit bloks with 128, 192, or 256 bit keys and is much harder to brute force than DES.
- In public key crypto, it should be impossible to compute the private key from the public key.
- The RSA key selection algorithm is given in 12.21, with the actual algorithm for encrypting+decrypting in 12.22.
- _Message Authentication Code (MAC)_ (13.6): Using a shared secret, compute a hash of the original data concatenated with the secret and append that to the original message.
    + This can be used as a digital signature.
- _Certificate Authority (CA)_: Verifies that a particular public key corresponds to a entity or business.
- Secure email process (confidential):
    1. Generate random symmetric private key
    2. Encrypt message with the symmetric key, and the symmetric key with the receivers public key
    3. The receiver uses his private key to get the symmetric key, then uses the symmetric key to recover the message.
    + The senders private key can also be used in an encryption step.

## SSL
- Provides transport layer security to a TCP-based application (runs on top of TCP between the transport and application layers).
- The SSL handshake process begins once a TCP connection is established.
- Creates a data encryption key and MAC key from a shared secret that is initially established during the handshake (13.19).
- The sequence number is not actually sent in the SSL record, but is used to compute the MAC.
- Keys are generated from the master key at both the client and the server.
- The sequence number is needed in SSL to verify that packets are not reordered or deleted.

## WEP
- Wired Equivalent Privacy was the first standard for encrypting wireless transmission (14.4).
- When the connection is startet, the host and AP create a 64 bit key (40 bit symmetric key + 24 bit initialization vector)
- The Key is used to generate a stream of keys k_i.
- k_i is then used to encrypt the i'th frame.
- Only the initialization vector is sent in the frame, the shared symmetric key is known to both the host and AP.  The initialization vector is sent in plaintext as it must be combined with the secret key to encrypt/decrypt a packet.
- WEP does not provide message integrity.

## WPA/WPA2
- WPA uses a 48 bit IV compared to the 24 bits in WEP.
- Uses a 128 bit key compared to the 40 bits in WPA.
- Includes a 64 bit checksum
- Adds a sequence counter to prevent replay attacks.
- AES is used in WPA2 rather than RC4

## Firewalls
- Provide a means of filtering packets from being routed.
- Stateless filtering decides weather or not to forward based on source/destination IP, port numbers, message types, or other header flags.
- Stateful filters can use history to make forwarding decicions.
    + A stateful filter could only route packets in TCP if a connection was already established.
- Application gateways can be used to filter packets based on application data.
    + Only specific users could be allowed to telnet outside.
    + An SSH gateway could be established that is the only means of entry into a system.
- An IDS (Intrusion Detection System) does deep packet inspection and looks at packet contents.  It can also examine the correlation between multiple packets.

## General Routing
- _Inter-AS_: Admin wants control over how its traffic is routed and who routes through its network.
- _Intra-AS_: Single admin, so no policy decisions are needed.
- In general, intra-AS focuses on performance while inter-AS needs to account for policy.
- Intra-AS includes RIP, OSPF, while Inter-AS is basically BGP.
- AS Types (17.11):
    + Stub AS: Only one connection to another AS (no through traffic).
    + Multihomed AS: More than one connection to other ASs, but still has no through traffic.
    + Transit AS: More than one connection to another AS, and supports transit traffic.

## RIP (Routing Information Protocol)
- Message format given on 15.21.
- Uses a distance vector algorithm, which means that a single host sends its distances to other hosts.
- Distance vectors are advertised every 30 seconds, and each advertisement can send up to 25 destination networks.
- If no advertisement is heard after 180 seconds, a host is declared dead.  This causes new advertisements to be sent by neighbors and the changes propagate quickly.
    + The hoest is not immediately removed.  A garbage collector timer is started and until that completes, the entry for the lost host is set to infinity (16).
- Uses UDP over port 520.
- Disadvantages of RIP:
    + Slow convergence due to the distance vector algorithm.
    + Routing looks can occur, along with a count to infinity problem (see slide 15.13).
    + Number of hops limited to 15, as infinity is assumed to be 16.
    + Only supports the hop-count metric.
- RIPv2 has several new features:
    + Support for classless addressing and subnet mask specification.
    + Authenticaion and security
    + Routing advertisements can be multicast instead of broadcast.
    + It can be used with IPv6.
- In linux, RIP is implemented with *routed*.
- Loops can potentially occur when a link goes down, and one host knows about it, but another doesn't.  If the link A->C drops and A sets C to 16 then receives an update from B using false information for the cost of C, A will believe it can reach C through B and propagate this information.  Eventually a routing loop will occur and both costs will go to infinity.
- Fixes for problems with RIP:
    + _split horizon_: A router only advertises the routes to each neighbor that don't have that neighbor as the first hop.
    + _poisoned reverse_: All routes are advertised to each neighbor, but routes geing through that neighbor as the first hop are given a distance of 16.
    + _holddown timer_: A timer on a router that starts when a host becomes unreachable, and until the timer expires, the router will discard all data saying that the unreachable host is actually reachable somewhere else.
    + _triggered updates_: As soon as a network change is detected (such as a host going down), RIP immediately sends an advertisement to notify neighbors of this change.
- While the distance vector algorithm of RIP usually uses hops as the metric, another metric could also be used as well as all link costs are positive.

## OSPF (Open Shortest Path First)
- Uses a link state algorithm, which means a topology map at each node is transmitted.
- Does route computation using Dijkstra's.
- Advertisements are sent to the entire autonomous system via flooding, and carry one entry per each neighbor router.
- Hierarchial OSPF:
    + Link-state advertisements are only sent in specific area.
    + A *backbone* area is included as well that has borders to all the other areas.
    + The combination of the routes from the outside areas and the backbone will allow packets to be routed.
- *Link Types*:
    + Point-to-point: Point-to-point connection to another router.
    + Transient: A link that connects to many routers (very common).
    + Stub: A link that is connected to no other routers.  A stub link cost is not included in the direction into the stub network, as the idea behind a stub is that no transit traffic should ever be passing through the link.
    + Virtual: A link that is configured by an administration that spans multiple physical links.  These are typically configured by administrators to connect routers into the backbone that aren't phsically connected, and need to connect over multiple physical links.
- OSPF Messages:
    + Hello message: Exchanged in the first step of routing to know weather a neighbor is alive and reachable. All routers exchange hello messages with their neighbors.
    + Database description message: Sent by neighbors when they hear from a router for the first time.  It provides the outline of each link without details than an LSA would include.
    + Link state request messages: A request for a link state update packet.
    + Link state update message: Communicates link state advertisements (LSA) to another neighbor.
- OSPF runs on top of IP with no transport layer protocol.
- The OSPF header uses a one's complement checksum.
- OSPF only advertises link costs of its outgoing links.
- When receiving a link state update message, a router only flood it to neighboring designated routers.

## BGP (Border Gateway Protocol)
- An inter-AS routing protocol.
- Three types of relationships/hosts in BGP:
    + _Providers_: Typically an ISP.
    + _Consumers_: A user that pays a provider for access to the internet.
    + _Peers_: Allows connectivitiy between two consumers.  They do not provide transit between peers, or exchange money.
- BGP Export Rules:
    + Do not advertise provider or peer routes to other providers or peers.
    + Advertise all routes to customers.
    + Advertise all customer routes to providers and peers.
- BGP Imports routing information based on the priority Customer -> Peer -> Provider, or more detailed preferences can bet set using the LOCAL_PREF attribute.
- Pairs of routers communicate over BGP on top of TCP port 179 (18.9).
- BGP sessions may not happen over a physical link.
- _eBGP_ is used between routers in different ASs.  They can exchange prefixes and implement routing policies.
- _iBGP_ is used between routers within a single AS.  This can transmit local prefixes to be sent outside through eBGP.
- iBGP is needed over RIP or OSPF because BGP sends a richer set of attributes.
- Internal routes sent over iBGP are not distributed to other peers.
- _BGP Path Attributes_ in order of preference (18.12) (see 18.22 for preference):
    + LOCAL_PREF: A numerical value set in an LSA that indicates the preference of a route compared to others.
    + AS_PATH: A list of AS numbers through which an advertisement has already traveled.  The sending AS will always be in this list.  The metric is the length of this list.
    + MED (multi-exit discriminator): A numerical metric to indicate which router is best for entering a specific AS.  The lower the value, the more prefereable.
    + ORIGIN: Either IBGP, EBGP, or Incomplete when a route comes from some other source.  Prefer an external over an internal path.
    + NEXT_HOP: The IP address of the most recent advertising router using eBGP.  Prefer the lowest IP address.
- The BGP keepalive message just inidicates to neighbors that a host is alive.
- A notification message is sent on an error or to close a connection.
    + When a notification is received with "Hold Timer Expired" it means one host wants to close the connection.  The other host then responds with a TCP segment with the "RST" flag set to indicate that the connection is over.
