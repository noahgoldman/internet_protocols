Programming Assignment 2 by Noah Goldman

The programs are working with the features described on Linux.

Usage:
- Modify the first line of the "main" function in "src/com/box/SyncClient.java" to point the client at the appropriate IP address of the server.  The default is the loopback address.
- If needed, replace "Configuration.SERVER_PORT" in either SyncClient.java or SyncServer.java to be the desired directory to sync.
- Build the programs with "make"
- Run the server code with "./run s"
- Run the client code with "./run c"

Features Implemented:
- The selective synchronization behavior described in the assignment prompt.  Specify a comma-separated list of file extensions with the "-t" option for files to sync, or a comma-separated with with the "-n" option for file extensions not to sync (to the client).
    Example: "./run c -n txt,pdf"
             "./run c -t bin"
- Automatic resyncing on changes in the synced folder.  Both the client and server programs continuously run, and when a change in the client directory is detected, a sync is performed.  These changes include creating, deleting, and modifying a file, along with similar operations such as moving, copying, etc...
