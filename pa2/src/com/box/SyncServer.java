package com.box;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;
import com.box.Configuration;

public class SyncServer {

    private ServerSocket serverSock;
    private Socket clientSock;
    private InputStreamReader inFromClient;

    // Directory to sync to
    private final String directory;

    public SyncServer() {
        this(Configuration.DEFAULT_SYNC_DIR);
    }

    public SyncServer(String directory) {
        this.directory = directory;
    }

    public void Init() {
        try {
            serverSock = new ServerSocket(Configuration.SERVER_PORT);
            System.out.println ("TCPServer Waiting for client on port "+serverSock.getLocalPort());
        } catch (IOException e) {
            System.out.println(e);
        }

        // Create the box directory
        new File(this.directory).mkdirs();
    }

    public static void main(String argv[]) throws Exception {
        SyncServer s = new SyncServer(Configuration.DEFAULT_SYNC_DIR);
        s.Init();

        while (true) {
            s.Sync();
        }
    }

    public void Sync() throws IOException {
        this.clientSock = this.serverSock.accept();
        this.inFromClient = new InputStreamReader(this.clientSock.getInputStream());

        System.out.println("Beginning sync with client");

        String fromClient = this.getMessage();

        String toClient;
        try {
            toClient = this.createFileStructure();
        } catch (IOException err) {
            System.out.println(err);
            return;
        }

        this.sendMessage(toClient, 0);

        while(true) {
            fromClient = this.getMessage();
            String [] temp = fromClient.split("\\*\\*");
            if(temp[0].compareTo("REQUEST")==0)
                this.sendMessage(fromClient, 1);
            else if(temp[0].compareTo("UPDATE")==0)
                this.sendMessage(fromClient, 2);
            else if(temp[0].compareTo("CREATE")==0)
                this.sendMessage(fromClient, 3);
            else if(temp[0].compareTo("DONE")==0)
            {
                System.out.println("THANKS!!!!\n");
                break;
            }

        }
    }

    public String getMessage()
    {
        //System.out.println("Entering getMessage!!!");
        String fromClient = new String();
        try {
            int c;
            while ((c = inFromClient.read()) != -1) {
                if (c == 0x0A) break;
                fromClient = fromClient.concat(String.valueOf((char)c));
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        //System.out.println("Exiting getMessage() with fromClient = "+fromClient);
        return fromClient;
    }

    public String createFileStructure() throws IOException
    {
        //		 System.out.println("Entered createFileStructure!!");
        File dirFile = new File(directory);
        String [] fileList = new String[100];
        fileList = dirFile.list();

        String toClient = new String();

        for(int i=0;i<fileList.length;i++)
        {
            File file = new File(directory+"//"+fileList[i]);
            toClient=toClient.concat(fileList[i]+"$"+file.lastModified()+"#");
        }

        //		 System.out.println("Quitting createFileStructure with toClient = "+toClient);

        return toClient;
    }

    public void sendMessage(String fromClient, int flag)
    {
        try {
            String toClient = new String();
            String [] temp = new String[100];

            if(flag == 0)
            {
                toClient = fromClient;
            }

            if(flag==1)
            {
                temp = fromClient.split("\\*\\*");

                if(temp[0].compareTo("REQUEST")==0)
                {
                    File src = new File(directory+"//"+temp[1]);
                    FileInputStream fis = new FileInputStream(src);

                    int c;
                    while ((c = fis.read()) != -1) {
                        toClient = toClient.concat(String.valueOf((char)c));
                    }
                }
            }

            if(flag==2)
            {
                temp = fromClient.split("\\*\\*");

                String fromClientFile = new String();
                String tempTimeStamp = new String();
                long timeStamp = System.currentTimeMillis();

                try {
                    int c;
                    while ((c = inFromClient.read()) != -1) {
                        if((char)c == '~')
                        {
                            while((c = inFromClient.read())!=-1)
                            {
                                if((char)c == '`')
                                {
                                    timeStamp = Long.parseLong(tempTimeStamp);
                                    break;
                                }
                                tempTimeStamp = tempTimeStamp.concat(String.valueOf((char)c));
                            }
                            break;
                        }
                        fromClientFile = fromClientFile.concat(String.valueOf((char)c));
                    }
                } catch (IOException e) {
                    System.out.println(e);
                }

                File newFile = new File(directory+"//"+temp[1]);
                DataOutputStream dos = new DataOutputStream(new FileOutputStream(newFile));
                dos.writeBytes(fromClientFile);
                dos.close();
                newFile.setLastModified(timeStamp);
            }

            if(flag==3)
            {
                temp = fromClient.split("\\*\\*");

                sendMessage("SEND", 0);

                String fromClientFile = new String();
                String tempTimeStamp = new String();
                long timeStamp = System.currentTimeMillis();

                try {
                    int c;
                    int entered=0;
                    while ((c = inFromClient.read()) != -1) {
                        if((char)c == '~')
                        {
                            entered=1;
                            continue;
                        }
                        if(entered==1)
                        {
                            tempTimeStamp = tempTimeStamp.concat(String.valueOf((char)c));
                            if((char)c == '`')
                            {
                                entered = 2;
                                //System.out.println("c in while == "+c+" Breaking!!!");
                            }
                        }
                        if(entered==0)
                        {
                            fromClientFile = fromClientFile.concat(String.valueOf((char)c));
                        }
                        if(entered==2)
                        {
                            tempTimeStamp = tempTimeStamp.substring(0, tempTimeStamp.length()-1);
                            timeStamp = Long.valueOf(tempTimeStamp);
                            break;
                        }
                    }
                } catch (IOException e) {
                    System.out.println(e);
                }

                File newFile = new File(directory+"//"+temp[1]);

                boolean ret = newFile.createNewFile();

                DataOutputStream dos = new DataOutputStream(new FileOutputStream(newFile));
                dos.writeBytes(fromClientFile);
                dos.close();
                newFile.setLastModified(timeStamp);
            }
            if(flag==0 || flag==1)
            {
                PrintWriter outToClient =
                    new PrintWriter(clientSock.getOutputStream(),true);
                if(flag==1)
                    toClient = toClient.concat("~");
                outToClient.println(toClient);
            }
            //			 System.out.println("SENT");
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
