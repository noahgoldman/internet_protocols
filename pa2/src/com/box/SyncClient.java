package com.box;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.WatchService;
import java.nio.file.WatchKey;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import java.util.HashMap;

import com.box.Configuration;

public class SyncClient {

    // Host and port for the client to connect to
    private final String host;

    private final String directory;

    // A watcher for the directory
    private WatchService watcher;

    // Watchkeys
    private Map<WatchKey, Path> watchKeys;

    // Allowed extensions for the client to sync
    private String[] allowedExtensions = null;

    // Optionally set single file to transfer
    private String fileToTransfer = null;

    Socket clientSocket;

    public SyncClient(String host) {
        this(host, Configuration.DEFAULT_SYNC_DIR);
    }

    public SyncClient(String host, String directory) {
        this.host = host;
        this.directory = directory;
    }

    public void Init() {
        try {
            this.watcher = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            System.out.println(e);
        }

        this.watchKeys = new HashMap<>();

        // Create the box directory
        new File(this.directory).mkdirs();
    }

    public void OpenSocket() {
        try {
            clientSocket = new Socket(this.host, Configuration.SERVER_PORT);
        } catch (UnknownHostException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void CloseSocket() throws IOException {
        if (clientSocket != null) {
            clientSocket.close();
            clientSocket = null;
        }
    }

    public static void main(String argv[]) throws Exception {
        SyncClient c = new SyncClient("127.0.0.1", Configuration.DEFAULT_SYNC_DIR);

        // Parse command line arguments
        for (int i = 0; i < argv.length; i++) {
            // Allowed extensions option
            if (argv[i].equals("-t")) {
                // split the extensions
                c.allowedExtensions = argv[++i].split(",");
            } else if (argv[i].equals("-n")) {
                c.fileToTransfer = argv[++i];
            }
        }

        c.Init();

		// Do an initial sync
		c.Sync();

        c.WatchSync();
    }

    public void Sync() throws IOException {
        this.OpenSocket();
        this.sendMessage("GET");

        String fromServer = this.getMessage();

        this.listFiles(fromServer);

        System.out.println("THANKS!!!");
        this.sendMessage("DONE");
        System.out.println("BYE!!!\n");
        this.CloseSocket();
    }

    public void WatchSync() throws IOException {
        this.registerWatches();
        while (true) {
            try {
                WatchKey watchKey = this.watcher.take();
                watchKey.pollEvents();

				System.out.println("Resyncing due to detected change");
                Sync();

                watchKey.reset();
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    private void registerPath(Path dir) throws IOException {
        WatchKey key = dir.register(this.watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        this.watchKeys.put(key, dir);
    }

    private void registerWatches() {
        try {
            Path root = FileSystems.getDefault().getPath(this.directory);
            Files.walkFileTree(root, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    registerPath(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void sendMessage(String toServer)
    {
        //System.out.println("Entering sendMessage with toServer = "+toServer);
        try {
            PrintWriter outToServer = new PrintWriter(clientSocket.getOutputStream(),true);
            outToServer.println(toServer);
            //			System.out.println("Quitting sendMessage!!!");
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public String getMessage()
    {
        //System.out.println("Entering getMessage!!!");
        String fromServer = null;
        try {
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(
                        clientSocket.getInputStream()));
            fromServer = inFromServer.readLine();
        }  catch (IOException e) {
            System.out.println(e);
        }
        //System.out.println("Quitting getMessage() with fromServer = "+fromServer);
        return fromServer;
    }

    public void listFiles(String fileStructure)
    {
        //		System.out.println("Entering compareFiles");
        String [] files = new String[100];
        String [] fileNamesArr = new String[100];
        long [] timeStampArr = new long[100];

        files = fileStructure.split("#");

        for(int j=0;j<files.length;j++)
        {
            if(files[j]!=null)
            {
                String [] tempFile = new String[100];

                tempFile = files[j].split("\\$");

                if(tempFile.length == 1)
                {
                    break;
                }
                if (!checkIfAllowedFile(tempFile[0])) {
                    continue;
                }
                fileNamesArr[j] = tempFile[0];

                timeStampArr[j] = Long.valueOf(tempFile[1]);


                File dirFile = new File(this.directory);

                compareFiles(fileNamesArr[j], timeStampArr[j]);
            }
        }

        checkClientFiles(fileStructure);

        //		System.out.println("Quitting listFiles()");
    }

    public int compareFiles(String serverFileName, Long serverTimeStamp)
    {
        File file = new File(directory+"//"+serverFileName);
        String toServer = new String();

        if(!file.exists())
        {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println(e);
            }
            //			System.out.println("Local Copy doesn't exist. Request for server copy!!! "+serverFileName);
            toServer = "REQUEST**"+serverFileName;
            sendMessage(toServer);

            InputStreamReader inFromServer = null;

            try {
                inFromServer = new InputStreamReader(
                        clientSocket.getInputStream());
            } catch (IOException e1) {
                System.out.println(e1);
            }

            String fromServer = new String();
            try {
                int c;
                while ((c = inFromServer.read()) != -1) {
                    if((char)c == '~')
                    {
                        break;
                    }
                    fromServer = fromServer.concat(String.valueOf((char)c));
                }
            } catch (IOException e) {
                System.out.println(e);
            }
            try {
                DataOutputStream dos = new DataOutputStream(new FileOutputStream(file));
                dos.writeBytes(fromServer);
                dos.close();
                file.setLastModified(serverTimeStamp);
            } catch (Exception e) {
                System.out.println(e);
            }

            return 1;
        }

        else if(file.exists())
        {
            if(serverTimeStamp>file.lastModified())
            {
                try {
                    //					System.out.println("Server copy is the latest!!! "+serverFileName);
                    toServer = "REQUEST**"+serverFileName;

                    sendMessage(toServer);

                    InputStreamReader fileFromServer =
                        new InputStreamReader (clientSocket.getInputStream());

                    String fromServer = new String();

                    try {
                        int c;
                        while ((c = fileFromServer.read()) != -1) {
                            if((char)c == '~')
                            {
                                break;
                            }
                            fromServer = fromServer.concat(String.valueOf((char)c));
                        }
                    } catch (IOException e) {
                        System.out.println(e);
                    }

                    File newFile = new File(directory+"//"+serverFileName);
                    DataOutputStream dos = new DataOutputStream(new FileOutputStream(newFile));
                    dos.writeBytes(fromServer);
                    dos.close();
                    newFile.setLastModified(serverTimeStamp);
                } catch (FileNotFoundException e) {
                    System.out.println(e);
                } catch (IOException e) {
                    System.out.println(e);
                }
            }

            else if(serverTimeStamp<file.lastModified())
            {
                String toServerFile = new String();
                //				System.out.println("Client's copy is the updated one!!!");
                sendMessage("UPDATE**"+serverFileName);

                try {
                    File src = new File(directory+"//"+serverFileName);
                    FileInputStream fis = new FileInputStream(src);

                    int c;
                    while ((c = fis.read()) != -1) {
                        toServerFile = toServerFile.concat(String.valueOf((char)c));
                    }
                    toServerFile = toServerFile.concat("~");
                    toServerFile = toServerFile.concat(String.valueOf(src.lastModified()));
                    toServerFile = toServerFile.concat("`");
                    sendMessage(toServerFile);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        else
        {
            //			System.out.println("MATCH!!! "+serverFileName);
        }
        //		System.out.println("Quitting compareFiles with toServer = "+toServer);
        return 0;
    }

    public void checkClientFiles(String fileStructure)
    {
        //		System.out.println("Entering checkClientFiles");
        String [] files = new String[100];
        String [] fileNamesArr = new String[100];
        long [] timeStampArr = new long[100];

        File dirFile = new File(directory);
        String [] fileList = new String[100];
        fileList = dirFile.list();
        files = fileStructure.split("#");

        for(int i=0;i<fileList.length;i++)
        {
            if (!checkIfAllowedFile(fileList[i])) {
                continue;
            }

            int flag=0;
            for(int j=0;j<files.length;j++)
            {
                if(files[j]!=null)
                {
                    String [] tempFile = new String[100];

                    tempFile = files[j].split("\\$");

                    if(tempFile.length == 1)
                    {
                        break;
                    }

                    fileNamesArr[j] = tempFile[0];

                    timeStampArr[j] = Long.valueOf(tempFile[1]);

                    if(fileList[i].compareTo(fileNamesArr[j]) == 0)
                    {
                        flag=1;
                        break;
                    }
                }
            }
            if(flag==1)
            {
                //				System.out.println("Client: Match "+fileList[i]);
            }
            else
            {
                //				System.out.println("Send file to Server!!! file == "+fileList[i]);
                String toServerFile = new String();
                sendMessage("CREATE**"+fileList[i]);

                String getMsg = getMessage();
                if(getMsg.equals("SEND"))
                {
                    try {
                        File src = new File(directory+"//"+fileList[i]);
                        FileInputStream fis = new FileInputStream(src);

                        int c;
                        while ((c = fis.read()) != -1) {
                            toServerFile = toServerFile.concat(String.valueOf((char)c));
                        }
                        toServerFile = toServerFile.concat("~");
                        toServerFile = toServerFile.concat(String.valueOf(src.lastModified()));
                        toServerFile = toServerFile.concat("`");
                        sendMessage(toServerFile);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
        }
    }

    // Check if a filename matches one of the allowed extensions.  If
    // allowedExtensions is null, just return true
    private boolean checkIfAllowedFile(String fname) {
        if (this.fileToTransfer != null) {
            return fname.equals(this.fileToTransfer);
        }

        if (this.allowedExtensions == null) {
            return true;
        }

        for (int i = 0; i < this.allowedExtensions.length; i++) {
            if (fname.endsWith(this.allowedExtensions[i])) {
                return true;
            }
        }

        return false;
    }
}
