package com.box;

public class Configuration {
    static final int SERVER_PORT = 57234;

    static final String DEFAULT_SYNC_DIR = "box";
}
